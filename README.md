# DSSSGGEC LaTeX Workshop


This repository contains solutions to assignment given in the LaTeX Workshop.

### Usage
The Solutions are stored in three folders
1. A1
2. A2
3. A3

All the solution tex files start with the prefix **RWOL853**.
Images used in the assignment are also provided in the same folders as the .tex file.
